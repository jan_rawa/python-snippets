import numpy as np
import sympy as sp
from sympy.utilities.lambdify import lambdify
from IPython.display import display, Math, Latex
def u_f(func, varbs, latex = True):
    #example1 u_f(r'\alpha', [a1, a2])
    #example2 u_f(sympy_expression, [a1, a2])
    texstr = r'\sqrt{'
    #for creating definitions (without calculationg derivatives)
    if type(func) == str:
        for i in range(len(varbs) - 1):
            texstr += r'\left(\frac{\partial ' + func + r'}{\partial ' + str(varbs[i]) + r'} \cdot u(' + str(varbs[i]) + r') \right)^2' + ' + '
        texstr += r'\left(\frac{\partial ' + func + r'}{\partial ' + str(varbs[len(varbs) - 1]) + r'} \cdot u(' + str(varbs[len(varbs) - 1]) + r') \right)^2}'
    #for creating full expresions (calculated derivatives included)
    else:
        #symbols
        for i in range(len(varbs) - 1):
            texstr += r'\left( ' + sp.latex(sp.diff(func, varbs[i])) + r' \cdot u(' + str(varbs[i]) + r') \right)^2' + ' + '
        texstr += r'\left( ' + sp.latex(sp.diff(func, varbs[len(varbs) - 1])) + r' \cdot u(' + str(varbs[len(varbs) - 1]) + r') \right)^2}'
    
    if latex:
        #displays Math with Markdown in LaTeX
        display(Math(texstr))
    else:
        #print raw LaTeX code
        print(texstr)

def u_f_num(func, numerical, partials = False, latex = False):
    #example u_f(sympy_expression, [[a1, a1_value, a1_uncertainty], [a2, a2_value, a2_uncertainty]])
    #warning if one of the numericals is a constant its uncertainty should equal 0
    ans = 0
    symbols = []
    values = []
    parts = []
    #numericals
    for i in range(len(numerical)):
        #partial diff of function with its num value
        temp = sp.diff(func, numerical[i][0]) * numerical[i][2]
        ans += (temp)**2
        #split symbols and values to their own lists
        symbols.append(numerical[i][0])
        values.append(numerical[i][1])
        #creating list of partial uncertanties values
        parts.append(temp)
    #create function out of sympy expresion
    ans = lambdify(symbols, ans)
    #calculates partial uncertanties as functions
    if latex or partials:
        for i in range(len(parts)):
            parts[i] = lambdify(symbols, parts[i])
    #prints partial values
    if partials:
        for i in range(len(parts)):
            print(symbols[i], parts[i](*values))
    #prints latex code of values in sqrt(()^2 + ()^2 + ...)) format
    if latex:
        texstr = r'\sqrt{'
        for i in range(len(parts) - 1):
            #if statement as a quick dirty fix of (0)^2 issue
            if parts[i](*values) != 0:
                texstr += r'(' + str(parts[i](*values)) + r')^2' + r' + '
        #if statement as a quick dirty fix of (0)^2 issue (more of the same as above)
        if parts[len(parts) - 1](*values) != 0:
            texstr += r'(' + str(parts[len(parts) - 1](*values)) + r')^2' + r'}'
        print(texstr)
    print(np.sqrt(ans(*values)))
