#!/usr/bin/python3
# PyVISA tool for transferring oscillosope screenshots over serial
# polprog 2021
# released on 3 clause BSD license
import pyvisa
from visa import *
import sys

#-----------------------------------------------------------
# configuration data - edit these variables
#-----------------------------------------------------------
#VISA path for the target device (see PyVISA documentation)
device = 'ASRL/dev/ttyS0::INSTR'

#Command for the target device (see manual)
command = ':DISPLAY:DATA? TIFF,SCR'

#offset for the data returned by device
offset = 10
#-----------------------------------------------------------


if len(sys.argv) < 2:
    print("Usage: " + str(sys.argv[0]) + " <filename.tiff>")
    sys.exit(1)

# Initialize PyVISA and print available devices
rm = pyvisa.ResourceManager()
print("Available devices" + str(rm.list_resources()))

#Try to open the device
try:
    print(f"opening {device}...", end='')
    scope = my_instrument = rm.open_resource(device)
    print(scope)
except ValueError:
    sys.exit(-1)
scope.timeout = 5000
f = open(sys.argv[1], 'wb')
bytes_read = 0;
screen = bytearray()
#Send the screenshot command
scope.write(command)
while 1:
    chunk = scope.read_raw()
    bytes_read += len(chunk)
    # the oscilloscope does not signal end of data in any way
    # this is not the best solution - we stop on receive timeout
    if len(chunk) == 0:
        break; 
    screen += chunk
    print(str(bytes_read)+"...", end='', flush=True)
f.write(screen[offset:]) #trim the output data, first 10 bytes are not valid TIFF
f.close()
print("\nRead " + str(bytes_read) + " bytes total. Exiting...")
